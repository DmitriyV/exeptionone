package Logic;

/**
 * Created by ���� on 18.10.2015.
 */
public enum Days {

    MONDAY("This is monday!"),
    TUESDAY("This is tuesday!"),
    ENVIRONMENT("This is environment!"),
    THURSDAY("This is thursday!"),
    FRIDAY("This is friday!"),
    SATURDAY("This is saturday!"),
    SUNDAY("This is sunday");


    private String message;

    Days(String message){
        this.message = message;
    }

    public void messageDay(){
        System.out.println(this.message);
    }
}
