package Logic;

import java.util.Scanner;

/**
 * Created by ���� on 18.10.2015.
 */
public class Keyboard {

    public Keyboard() {
        //scannerDays();
    }

    public void scannerDays(){
        Scanner scanner = new Scanner(System.in);
        System.out.print("Insert day: ");
        String str = scanner.nextLine();
        scan(str);
    }

    private void scan(String day){
        int i = 0;

        for(Days days : Days.values()){
            if(days.name().equalsIgnoreCase(day)){
                days.messageDay();
                i++;
                break;
            }
        }

        if(i == 0){
            throw new IllegalArgumentException("Incorrect day!");
        }
    }

}
